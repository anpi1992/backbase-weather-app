var fs = require("fs");
require('dotenv').config();

// Configure Angular `environment.ts` file path
const targetPath = './src/environments/environment.ts';

// Load node modules
const colors = require('colors');
// `environment.ts` file structure
const envConfigFile = `export const environment = {
	 apiKey: '${process.env.METEO_API_KEY}',
	 production: '${process.env.production}'
};
`;
console.log(colors.magenta('The file `environment.ts` will be written with the following content: \n'));

console.log(colors.grey(envConfigFile));

fs.writeFile(targetPath, envConfigFile, (err) => {
	if (err) {
		throw console.error(err);
	} else {
		console.log(colors.magenta(`Angular environment.ts file generated correctly at ${targetPath} \n`));
	}
});
