# Backbase Weather app

## Implementation

Implementation of a simple Angular application that shows information about the weather in five European cities. 
The information about the current weather is retrieved from:
```
api.openweathermap.org/data/2.5/weather
api.openweathermap.org/data/2.5/forecast
```
The `environment.ts` file is edited by the npm task `config` (`set-env.js`) that reads the environment variables value from a `.env` file and saves it to the `enviroment.ts` that gets imported in the application and webpack configuration.

Clicking on a city shows:

 - a list of days with some weather information;
 - a list of hours of the selected day. For each hour the components show the weather and the temperature.
 - a line chart (in Angular Bootstrap) that renders the difference between the maximum and the minimum temperature during the selected day.

You can switch between the days by clicking on the `DayButton` components.

Details:
 - The styles have been created using flexbox and the final result should be responsive for smartphones and tablets. There are also a few media queries in the SCSS files.
 - The chart library is Material Design for Bootstrap (https://mdbootstrap.com/)
 - I've also handled some of the loading phases a rendering a loader (I used a weather icon with an infinite rotation CSS animation but I could have used a FontAwesome icon since I had to import the library to use the Angular Bootstrap Chart).
 - `moment` has been imported for date parsing and formatting.
 - All the business logic, if possible, has been moved outside components, inside custom services.





## COMPONENTS
`AppComponent`: 
This component renders some information and the list of cities.

`CityComponent`
Every city is rendered as a `city` component, that in the onInit phase loads the information about the current weather from the API and shows the retrieved value on the screen.

`WeatherIconComponent`:
This component that receives a `Weather` objects as a parameter and renders a weather icon.
I've used this library for the weather icons:
```
https://github.com/erikflowers/weather-icons/
```
There was no package for this repo so I had to import it in the `package.json` like this:
```
"weather-icons": "git://github.com/erikflowers/weather-icons.git"
```

`DayButtonComponent`
This button component that when clicked recalculate the forecast for the selected day of the week.

`HourComponent`
This component show the information about the weather at a certain hour of the selected day.

`ChartForecastComponent` component that renders the Bootstrap line chart, showing some detail about the min and the max temperature during the day.

## SERVICES

`WeatherService` all the API calls are inside this service that also adds the apiKey parameter to the url. It also contains a mapping function that maps the api result to a DTO to simplify the type checking (I didn't find any Typescript types for the openweathermap API's result, so I've created my own).

`ForecastService` 
This service exposes a list of functions to calculate the median weather status during the day and the median temperature.

`DeviceService` 
This generic service that detects the deviceType in use.

## PIPE

`IntegerPipe` to remove the floating part from the temperature values


## Details

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.8.

Start the ng serve 
```
npm install
npm run start 
```
