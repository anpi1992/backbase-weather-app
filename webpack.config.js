const webpack = require('webpack')
console.log('Load additing webpack configuration');
module.exports = {
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				apikey: JSON.stringify(process.env.METEO_API_KEY),
				production: JSON.stringify(process.env.production)
			}
		})
	]
}
