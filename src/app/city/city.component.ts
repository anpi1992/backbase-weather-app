import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit {
  @Input() cityName: string;
  @Input() selected: boolean;
  @Output() notifyToggle = new EventEmitter<string>();
  weather: any[];
  wind: any;
  info: any;
  weatherIcon: string;
  forecast: any;
  initialized: boolean;
  constructor(private ws: WeatherService) {
  }

  ngOnInit() {
    this.initialized = false;
    this.ws.getCurrentWeather(this.cityName)
      .subscribe((data: any) => {
        this.weather = data.weather;
        this.wind = data.wind;
        this.info = data.main;
        this.initialized = true;
      })
  }

  toggleForecast() {
    this.notifyToggle.emit(this.cityName);
  }
}
