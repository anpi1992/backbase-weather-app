import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor() { }

  public isSmartphone() {
    return window.innerWidth < 600;
  }
}
