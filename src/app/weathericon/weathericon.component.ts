import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-weathericon',
  templateUrl: './weathericon.component.html',
  styleUrls: ['./weathericon.component.css']
})
export class WeathericonComponent implements OnInit {
  @Input() size: Int16Array;
  @Input() weather: any;
  weatherIcon: string;
  iconStyle: any;
  constructor() { }

  ngOnInit() {
    this.iconStyle = {'font-size': this.size + 'px'};
    this.weatherIcon = this.getWeatherIcon();
  }

  private getWeatherIcon(): string {
    switch (this.weather.main) {
      case 'Thunderstorm':
        return 'wi-thunderstorm';
      case 'Drizzle':
        return 'wi-rain';
      case 'Rain':
        return 'wi-rain';
      case 'Snow':
        return 'wi-snow';
      case 'Mist':
        return 'wi-windy';
      case 'Smoke':
        return 'wi-smoke';
      case 'Haze':
        return 'wi-day-clear';
      case 'Dust':
        return 'wi-dust';
      case 'Fog':
        return 'wi-fog';
      case 'Sand':
        return 'wi-sandstorm';
      case 'Ash':
        return 'wi-volcano';
      case 'Squall':
        return 'wi-rain-wind';
      case 'Tornado':
        return 'wi-tornado';
      case 'Clear':
        return 'wi-day-sunny';
      case 'Clouds':
        return 'wi-cloudy';
    }
    return 'wi-day-sunny'
  }

}
