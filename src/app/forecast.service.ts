import { Injectable } from '@angular/core';
import { ForecastDay, ForecastHour } from './dto/ForecastDto';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {
  constructor() { }
  public getMostFrequentWeather(day: ForecastDay) {
    const weatherFrequencies = day.hours
      .reduce((acc, h) => ([...acc, ...h.meteoInfo.weather]), [])
      .map(w => w.main)
      .reduce((acc, w) => {
        if (acc.hasOwnProperty(w)) {
          acc[w] = acc[w] + 1;
        } else {
          acc[w] = 1;
        }
        return acc;
      }, {});

    const weatherOrderedByFrequency = Object.keys(weatherFrequencies)
      .sort((a,b) => weatherFrequencies[b] - weatherFrequencies[a]);
    return weatherOrderedByFrequency[0];
  }

  public minDayTemperature(day: ForecastDay) {
    const orderedHours = day.hours
      .sort((a: ForecastHour, b: ForecastHour) => a.meteoInfo.main.temp_min - b.meteoInfo.main.temp_min);
    return orderedHours[0].meteoInfo.main.temp_min;
  }

  public maxDayTemperature(day: ForecastDay) {
    const orderedHours = day.hours
      .sort((a: ForecastHour, b: ForecastHour) => b.meteoInfo.main.temp_max - a.meteoInfo.main.temp_max);
    return orderedHours[0].meteoInfo.main.temp_max;
  }
}
