import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { CityComponent } from './city/city.component';
import { ForecastComponent } from './forecast/forecast.component';
import { WeathericonComponent } from './weathericon/weathericon.component';
import { ChartForecastComponent } from './chart.forecast/chart.forecast.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { IntegerPipe } from './integer.pipe';
import { DayButtonComponent } from './day-button/day-button.component';
import { HourComponent } from './hour/hour.component';
@NgModule({
  declarations: [
    AppComponent,
    CityComponent,
    ForecastComponent,
    WeathericonComponent,
    ChartForecastComponent,
    IntegerPipe,
    DayButtonComponent,
    HourComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MDBBootstrapModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
