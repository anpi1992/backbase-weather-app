export interface ForecastDay {
	hours: ForecastHour[];
  description: string;
  dayOfWeek: string;
  selected: boolean;
	mostFrequentWeather: string;
	minimumTemperature: number;
	maximumTemperature: number;
}

export interface ForecastHour {
	description: string;
	meteoInfo: Weather;
}

export interface Weather {
	dt: number;
	main: { temp: number, temp_min: number, temp_max:number, pressure: number, sea_level: number, grnd_level: number, humidity: number, temp_kf: number },
	weather: WeatherInfo[];
	cloud: CloudInfo;
	wind: WindInfo;
	sys: { pod: string };
	dt_txt: string;
	mostFrequentWeather: string;
	minimumTemperature: number;
	maximumTemperature: number;
}


export interface WeatherInfo {
	id: number;
	main: string;//Clear",
	description: string;
	icon: string;
}
export interface CloudInfo {
	all: number;
}

export interface WindInfo {
	speed: number;
	deg: number;
}
