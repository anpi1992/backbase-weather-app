import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Weather, ForecastDay, WeatherInfo } from '../dto/ForecastDto';
// IMPORT BOOTSTRAP
@Component({
  selector: 'app-chartforecast',
  templateUrl: './chart.forecast.component.html',
  styleUrls: ['./chart.forecast.component.scss']
})
export class ChartForecastComponent implements OnInit, OnChanges {
  ngOnChanges(changes: SimpleChanges): void {
    this.initialize();
  }
  @Input() weather: ForecastDay;
  public chartLabels: Array<any>;
  public chartColors: Array<any>;
  public chartOptions: any;
  public chartType: string;
  public chartDatasets: Array<any>;
  ngOnInit(): void {
    this.initialize();
  }

  initialize() {
    this.chartType = 'line';
    this.chartDatasets = [
      { data: this.weather.hours.map(hour => hour.meteoInfo.main.temp_min), label: 'Min temperature' },
      { data: this.weather.hours.map(hour => hour.meteoInfo.main.temp_max), label: 'Max temperature' },
    ];
    this.chartLabels = this.weather.hours.map(hour => hour.description);
    this.chartColors = [
      {
        backgroundColor: 'rgba(255, 127, 80, 1)',
        borderColor: 'rgba(255, 127, 80,1)',
        borderWidth: 2,
      },
      {
        backgroundColor: 'rgba(205, 92, 92, 1)',
        borderColor: 'rgba(205, 92, 92, 1)',
        borderWidth: 2,
      }
    ];
    this.chartOptions = {
      responsive: true,
      legend: {
        display: false
      },
      tooltips: {
        enabled: false
      }
    };
  }
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }
}
