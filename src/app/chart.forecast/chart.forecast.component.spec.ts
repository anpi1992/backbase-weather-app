import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartForecastComponent } from './chart.forecast.component';

describe('ChartForecastComponent', () => {
  let component: ChartForecastComponent;
  let fixture: ComponentFixture<ChartForecastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartForecastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartForecastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
