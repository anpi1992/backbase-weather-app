import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { WeatherService } from '../weather.service';
import { ForecastHour, ForecastDay, Weather } from '../dto/ForecastDto';
import { ForecastService } from '../forecast.service';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit, OnChanges {
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('cityName')) {
      if (changes.cityName.previousValue !== changes.cityName.currentValue) {
        this.reloadForecast();
      }
    }
  }
  @Input() cityName: string;
  forecast: ForecastDay[];
  initialized: boolean;
  selectedDay: ForecastDay;

  constructor(private ws: WeatherService, private fs: ForecastService) { }

  ngOnInit() {
    this.reloadForecast();
  }

  selectDay(day: ForecastDay): void {
    const lastSelectedDay = this.forecast.find(d => d.selected);
    if(lastSelectedDay) {
      lastSelectedDay.selected = false;
    }
    day.selected = true;
    this.selectedDay = day;
  }

  reloadForecast() {
    if (!this.cityName) {
      this.forecast = null;
      this.initialized = true;
      return;
    }
    this.ws.getWeatherForecastMapped(this.cityName)
      .subscribe((forecast: ForecastDay[]) => {
        this.forecast = forecast.map((day: ForecastDay, index: number) => {
          day.selected = index === 0;
          day.mostFrequentWeather = this.fs.getMostFrequentWeather(day);
          day.minimumTemperature = this.fs.minDayTemperature(day);
          day.maximumTemperature = this.fs.maxDayTemperature(day);
          day.hours.sort((a,b) => a.meteoInfo.dt - b.meteoInfo.dt)
          return day;
        });
        this.selectedDay = this.forecast[0];
        this.initialized = true;
      })
  }

}
