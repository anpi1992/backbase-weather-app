import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ForecastDay } from '../dto/ForecastDto';

@Component({
  selector: 'app-day-button',
  templateUrl: './day-button.component.html',
  styleUrls: ['./day-button.component.scss']
})
export class DayButtonComponent implements OnInit {
  @Input() day: ForecastDay;
  @Output() notifySelection: EventEmitter<ForecastDay> = new EventEmitter<ForecastDay>();

  click() {
    this.notifySelection.emit(this.day);
  }

  constructor() { }

  ngOnInit() {

  }

}
