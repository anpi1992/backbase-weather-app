import { Component, OnInit, Input } from '@angular/core';
import { ForecastHour } from '../dto/ForecastDto';

@Component({
  selector: 'app-hour',
  templateUrl: './hour.component.html',
  styleUrls: ['./hour.component.scss']
})
export class HourComponent implements OnInit {
  @Input() hour: ForecastHour;

  constructor() { }

  ngOnInit() {
  }

}
