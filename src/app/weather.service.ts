import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { retry, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ForecastDay, Weather } from './dto/ForecastDto';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  appId: string = environment.apiKey;
  constructor(private http: HttpClient) { }

  /* Get the current weather of a city */
  getCurrentWeather(cityName: string) {
    return this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${cityName}&APPID=${this.appId}&units=Metric`)
    .pipe(
      retry(1)
    )
  }

  /* Get the weather forecast for a city */
  getWeatherForecast(cityName: string) {
    return this.http.get(`http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&APPID=${this.appId}&units=Metric`)
    .pipe(
      retry(1)
    )
  }

  /* Get the weather forecast for a city and map it */
  getWeatherForecastMapped(cityName: string){
    return this.http.get(`http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&APPID=${this.appId}&units=Metric`)
    .pipe(
      retry(1),
      map((data:any) => {
        const days = data.list;
        return days.reduce((result: ForecastDay[], item: Weather) => {
          const momentDate = moment(item.dt_txt);
          const dateString: string= momentDate.format('LL');
          const dateItem: ForecastDay = result.find(i => i.description === dateString);
          if (dateItem) {
            if(dateItem.hours.length < 7) {
              dateItem.hours.push({
                description: momentDate.format('HH'),
                meteoInfo: item
              });
            }
          } else {
            const forecastDay: ForecastDay = {
              description: dateString,
              dayOfWeek: momentDate.format('ddd'),
              hours: [{
                description: momentDate.format('HH'),
                meteoInfo: item
              }],
              minimumTemperature: 0,
              maximumTemperature: 0,
              mostFrequentWeather: null,
              selected: false
            };
            result.push(forecastDay)
          }
          return result;
        }, [])
      })
    )
  }


}
