import { Component } from '@angular/core';
import { WeatherService } from './weather.service';
import { DeviceService } from './device.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'backbase';
  cities: string[] = ['London', 'Paris', 'Rome', 'Amsterdam', 'Reykjavik']
  weatherForecast: string;
  isSmartphone: boolean;
  toggleCity(cityName: string) {
    if (this.weatherForecast === cityName) {
      this.weatherForecast = null;
    } else {
      this.weatherForecast = cityName;
      this.weatherForecast = cityName;
    }
  }

  constructor(private ws: WeatherService, device: DeviceService) {
    this.isSmartphone = device.isSmartphone();
   }
}
